<?php require_once('conecta.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="cache" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Jogo</title>
	<style>
		html,body {
			margin: 0;
			padding: 0;
			width: 100%;
			height: 100%;
			background-color: #000;
			background-image: url('bg.jpg');
			background-repeat: no-repeat;
			background-position: center bottom;
			background-size: cover;
			color: #fff;
			font-family: "Segoe UI", sans-serif;
			font-size:1.1em;
			font-weight: bold;
		}
		#container {

		}
		#logo {
			position: absolute;
			top: 200px;
			left:150px;
		}
		#scores {			
			width: 695px;
			height: 400px;
			padding: 60px 20px;
			background-image: url('bg-scores.png');
			background-repeat: no-repeat;
			background-position: center;
			background-size: cover;
			position: absolute;
			top:2%;
			right:20px;
		}
		#scores ul {
			overflow: auto;
			height:100%;
			width: 100%;
			margin:0;
			padding:0;
			list-style: none;
		}
		#scores ul li {
			float:left;
			width: 98%;
			border-bottom: 1px dashed #ccc;
			padding-top:4px;
		}
		#scores ul li span {
			float: right;
		}
	</style>
</head>
<body>
	<div id="container">
		<div id="logo">
			<img src="logo.png" alt="Logo">
		</div>
		<div id="scores">
			<ul>
			<?php
				$query = "SELECT * FROM records order by pontuacao desc";
				$result = mysql_query($query) or die (mysql_error());
				$cont = 1;

				while ($row = mysql_fetch_array($result)){
			?>
				<li><?php echo $cont ?> - <?php echo utf8_encode($row['nome']) ?> <span><?php echo $row['pontuacao'] ?></span></li>

			<?php $cont++; } ?>
			</ul>
		</div>
	</div>
</body>
</html>